using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Otus.Blog.Application;
using Otus.Blog.Domain;

namespace Otus.Blog.Infrastructure
{
  public class CategoryRepository : ICategoryRepository
  {
    private readonly BlogDbContext _db;

    public CategoryRepository(BlogDbContext db)
    {
      _db = db;
    }

    public Task<IEnumerable<Category>> GetCategories(int skip, int take)
    {
      var dbConnection = _db.Database.GetDbConnection();

      const string sql = "SELECT * FROM Categories C LIMIT @Take OFFSET @Skip";
      return dbConnection.QueryAsync<Category>(sql);
    }
  }
}