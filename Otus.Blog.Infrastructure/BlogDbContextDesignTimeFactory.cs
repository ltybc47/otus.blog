using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Otus.Blog.Infrastructure
{
  internal class BlogDbContextDesignTimeFactory : IDesignTimeDbContextFactory<BlogDbContext>
  {
    public BlogDbContext CreateDbContext(string[] args)
    {
      var optionsBuilder = new DbContextOptionsBuilder<BlogDbContext>();
      optionsBuilder.UseNpgsql(
        "Host=localhost;Port=32768;Database=Blog;Username=postgres");

      return new BlogDbContext(optionsBuilder.Options);
    }
  }
}