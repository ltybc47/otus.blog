﻿namespace Otus.Blog.Domain
{
  public class User
  {
    public int Id { get; set; }
    public string Login { get; set; }
  }
}