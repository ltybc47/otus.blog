namespace Otus.Blog.Domain
{
  public class Comment
  {
    public string Id { get; set; }
    public string Body { get; set; }
    public User Author { get; set; }
  }
}