using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Blog.Domain;

namespace Otus.Blog.Application
{
  public interface ICategoryRepository
  {
    Task<IEnumerable<Category>> GetCategories(int skip, int take);
  }
}