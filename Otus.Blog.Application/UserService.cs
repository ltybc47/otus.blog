using System;
using System.Threading.Tasks;
using Otus.Blog.Domain;

namespace Otus.Blog.Application
{
  public class UserService : IUserService
  {
    private readonly IUserRepository _userRepository;

    public UserService(IUserRepository userRepository)
    {
      _userRepository = userRepository;
    }

    public async Task<User> AddNewUser(User user)
    {
      if (user == null)
      {
        throw new ArgumentNullException(nameof(user));
      }

      // Здесь могла бы быть более сложная логика
      // для примера просто проверка, что логин ещё не занят

      var loginIsTaken = await _userRepository
        .GetByLogin(user.Login)
        .ContinueWith(x => x.Result != null);

      if (loginIsTaken)
      {
        throw new InvalidOperationException("Пользователь с таким логином уже существует");
      }

      return await _userRepository.Add(user);
    }
  }
}